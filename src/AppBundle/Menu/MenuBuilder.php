<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu()
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(['class' => 'nav nav-pills flex-column pt-4']);
        $menu->addChild('Главная', array('route' => 'dashboard_index'))
            ->setAttribute('class', 'nav-item')
            ->setAttribute('icon', 'fa fa-home')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('Контакты', [
            'route' => 'contacts_index',
            'extras' => [
                'routes' => [
                    [
                        'route' => 'contacts_person_show',
                    ],
                    [
                        'route' => 'contacts_person_edit',
                    ],
                    [
                        'route' => 'contacts_person_new',
                    ],
                ],
            ],
        ])
            ->setAttribute('class', 'nav-item')
            ->setAttribute('icon', 'fa fa-address-book')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('Задачи', [
            'route' => 'tasks_index',
            'extras' => [
                'routes' => [
                    [
                        'route' => 'tasks_show',
                    ],
                    [
                        'route' => 'tasks_edit',
                    ],
                    [
                        'route' => 'tasks_new',
                    ],
                ],
            ],
        ])
            ->setAttribute('class', 'nav-item')
            ->setAttribute('icon', 'fa fa-check')
            ->setLinkAttribute('class', 'nav-link');

        return $menu;
    }
}
