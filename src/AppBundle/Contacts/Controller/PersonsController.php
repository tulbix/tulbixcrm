<?php

namespace AppBundle\Contacts\Controller;

use AppBundle\Entity\Person;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Persons controller.
 *
 * @Route("contacts")
 */
class PersonsController extends Controller
{
    /**
     * Lists all persons entities.
     *
     * @Route("/", name="contacts_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $persons = $em->getRepository('AppBundle:Person')->findAll();

        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $persons,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 20)
        );

        return $this->render('contacts/persons/index.html.twig', array(
            'persons' => $result
        ));
    }

    /**
     * Creates a new persons entity.
     *
     * @Route("/persons/new", name="contacts_person_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $person = new Person();
        $person->setOwnerId($this->getUser());
        $person->setCreatedAt(new \DateTime('now'));
        $person->setUpdatedAt(new \DateTime('now'));

        $form = $this->createForm('AppBundle\Contacts\Form\PersonType', $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();

            return $this->redirectToRoute('contacts_person_show', array('id' => $person->getId()));
        }

        return $this->render('contacts/persons/new.html.twig', array(
            'person' => $person,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a persons entity.
     *
     * @Route("/persons/{id}", name="contacts_person_show")
     * @Method("GET")
     */
    public function showAction(Person $person)
    {
        $deleteForm = $this->createDeleteForm($person);

        return $this->render('contacts/persons/show.html.twig', array(
            'person' => $person,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing persons entity.
     *
     * @Route("/persons/{id}/edit", name="contacts_person_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Person $person)
    {
        $person->setUpdatedAt(new \DateTime('now'));

        $deleteForm = $this->createDeleteForm($person);
        $editForm = $this->createForm('AppBundle\Contacts\Form\PersonType', $person);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contacts_person_show', array('id' => $person->getId()));
        }

        return $this->render('contacts/persons/edit.html.twig', array(
            'person' => $person,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a persons entity.
     *
     * @Route("/persons/{id}", name="contacts_person_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Person $person)
    {
        $form = $this->createDeleteForm($person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($person);
            $em->flush();
        }

        return $this->redirectToRoute('contacts_index');
    }

    /**
     * Creates a form to delete a persons entity.
     *
     * @param Person $person The persons entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Person $person)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contacts_person_delete', array('id' => $person->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
