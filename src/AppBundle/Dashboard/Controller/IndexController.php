<?php

namespace AppBundle\Dashboard\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("dashboard")
 */
class IndexController extends Controller
{
    /**
     * @Route("/", name="dashboard_index")
     */
    public function indexAction()
    {
        return $this->render('dashboard/index.html.twig');
    }
}
